﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using WinFormsApplication;
using WinFormsApplication.Config;

namespace BHOSV002BETA
{
    public partial class resetpass : Form
    {
        private readonly string defPath = AppContext.Instance.UsersFile;

        public resetpass()
        {
            InitializeComponent();
        }
        private void CreateXMLDocument(string filepath) //создание xml файла
        {
            var xtw = new XmlTextWriter(filepath, Encoding.UTF32);
            xtw.WriteStartDocument();
            xtw.WriteStartElement("users");
            xtw.WriteEndDocument();
            xtw.Close();
        }

        private void resetpass_Load(object sender, EventArgs e)
        {
            int i;
            Random rnd = new Random();
            i = rnd.Next(0, 999999);
            label8.Text = i.ToString();
        }
        private void resetp_Click(object sender, EventArgs e)
        {
            if (TB1.Text == "")
            {
                l1.ForeColor = Color.Red;
                l1.Text = "Не был введен логин!";
            }
            else
            {
                l1.Text = "";
            }
            if (TB2.Text == "")
            {
                l2.ForeColor = Color.Red;
                l2.Text = "Не было введено имя!";
            }
            else
            {
                l2.Text = "";
            }
            if (TB3.Text == "")
            {
                l3.ForeColor = Color.Red;
                l3.Text = "Не была введена Фамилия!";
            }
            else
            {
                l3.Text = "";
            }
            if (TB4.Text == "")
            {
                l4.ForeColor = Color.Red;
                l4.Text = "Не был введен E-Mail!";
            }
            else
            {
                l4.Text = "";
            }
            if (TB5.Text == "")
            {
                l5.ForeColor = Color.Red;
                l5.Text = "Не был введен Город!";
            }
            else
            {
                l5.Text = "";
            }
            if (TB6.Text == "")
            {
                l6.ForeColor = Color.Red;
                l6.Text = "Не было введено слово проверки!";
            }
            else
            {
                l6.Text = "";
            }
            if (TB7.Text == "")
            {
                l7.ForeColor = Color.Red;
                l7.Text = "Не было введено ключевое слово(при регистрации)!";
            }
            else if (TB6.Text != label8.Text)
            {
                l7.Text = "";
                MessageBox.Show("Неверно введено проверочное слово!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                int i;
                Random rnd = new Random();
                i = rnd.Next(0, 9999);
                label8.Text = i.ToString();
                TB6.Text = "";
            }
            else
            {
                l7.Text = "";
                {
                    StreamWriter write_text; //Класс для записи в файл
                    var file2 = new FileInfo(TB1.Text + ".txt");
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("Login:" + TB1.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("Name:" + TB2.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("SurName:" + TB3.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("Mail:" + TB4.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("City:" + TB5.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("Answer word:" + TB7.Text);
                    write_text.Close(); // Закрываем файл
                    write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
                    write_text.WriteLine("---------------------------");
                    write_text.Close(); // Закрываем файл

                    var fileName = TB1.Text + ".txt";
                    var ftpServerIP = "31.220.16.128";
                    var ftpUserID = "u314821889";
                    var ftpPassword = "139713";
                    var fileInf = new FileInfo(fileName);
                    var uri = "ftp://" + ftpServerIP + fileInf.Name;
                    FtpWebRequest reqFTP;
                    reqFTP = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + ftpServerIP + "/resetpass/" + fileInf.Name));
                    reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.ContentLength = fileInf.Length;
                    var buffLength = 2048;
                    var buff = new byte[buffLength];
                    int contentLen;
                    var fs = fileInf.OpenRead();
                    try
                    {
                        var strm = reqFTP.GetRequestStream();
                        contentLen = fs.Read(buff, 0, buffLength);
                        while (contentLen != 0)
                        {
                            strm.Write(buff, 0, contentLen);
                            contentLen = fs.Read(buff, 0, buffLength);
                        }
                        strm.Close();
                        fs.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Upload Error");
                    }
                }
            }

            var rp = new resetpass();
            rp.Hide();
        }
    }
}

