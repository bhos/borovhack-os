namespace WinFormsApplication.Config
{
    /// <summary>
    /// ���������
    /// </summary>
    public class Const
    {
        /// <summary>
        /// ������ � ���� ����� �����
        /// </summary>
        public class FolderName
        {
            /// <summary>
            /// �������� �������� ����� ����������, ��������������� ��� �������� ������ ����������
            /// </summary>
            public const string Root = "DTO";

            public const string WallpaperCache = "WallpaperCache";
        }

        /// <summary>
        /// ������ � ���� ����� ������
        /// </summary>
        public class FileName
        {
            /// <summary>
            /// ��� ����� �������� ����������
            /// </summary>
            public const string SettingsFileName = "Settings.xml";

            /// <summary>
            /// ��� ����� �� ������� �������������
            /// </summary>
            public const string UsersFileName = "Users.xml";
        }
    }
}