﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WinFormsApplication;
using WinFormsApplication.Config;

namespace BHOSV002BETA
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LogInForm());
        }

        /// <summary>
        /// Обои
        /// </summary>
        public static Bitmap Wallpaper { get; set; }

        /// <summary>
        /// Инициализировать конфигурацию приложения. Проверить существование папок, если не существуют, создать.
        /// Попытаться загрузить файл с изображением обоев.
        /// </summary>
        public static void InitializeConfiguration()
        {
            AppContext.Instance.CheckFolders();
            LocalConfig.Instance.LoadCurrentSettings();
            TryLoadWallpaper(LocalConfig.Instance.Settings.WallpaperFile);
        }

        /// <summary>
        /// Попытаться загрузить обои из файла
        /// </summary>
        /// <param name="wallpaperFile">Файл с изображением</param>
        /// <returns></returns>
        public static bool TryLoadWallpaper(string wallpaperFile)
        {
            if (wallpaperFile != null && File.Exists(wallpaperFile))
            {
                Wallpaper = (Bitmap)Image.FromFile(wallpaperFile);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Если окно реализует интерфейс <see cref="IHaveWallpaper"/>, то обновить обои и показать окно.
        /// Если не реализует, то просто показать окно.
        /// </summary>
        /// <param name="form">Окно</param>
        public static void ShowForm(Form form)
        {
            UpdateWallpaperInternal(form);
            form.Show();
        }

        /// <summary>
        /// Если окно реализует интерфейс <see cref="IHaveWallpaper"/>, то обновить обои.
        /// </summary>
        /// <param name="form">Окно</param>
        public static void UpdateWallpaper(Form form)
        {
            UpdateWallpaperInternal(form);
        }

        /// <summary>
        /// Удалить все лишние закешированные изображения, кроме текущего выбранного.
        /// </summary>
        public static void ClearWallpaperCache()
        {
            LocalConfig config = LocalConfig.Instance;

            if (!File.Exists(config.Settings.WallpaperFile))
            {
                throw new FileNotFoundException("Актуальный файл обоев не найден", config.Settings.WallpaperFile);
            }

            IEnumerable<string> files = Directory.EnumerateFiles(AppContext.Instance.GetPath(Folder.WallpaperCache));

            foreach (string file in files)
            {
                if (file != config.Settings.WallpaperFile)
                {
                    File.Delete(file);
                }
            }
        }

        /// <summary>
        /// Кеширует изображение, загружает изображение в свойство <see cref="Wallpaper"/> 
        /// и возвращает путь к закешированному изображению
        /// </summary>
        /// <param name="sourceFile"></param>
        /// <returns></returns>
        public static string SetNewWallpaper(string sourceFile)
        {
            if (sourceFile == null)
            {
                throw new ArgumentNullException("sourceFile");
            }

            string fileName = Path.GetFileName(sourceFile);
            string destinationFile = Path.Combine(
                AppContext.Instance.GetPath(Folder.WallpaperCache), fileName);

            if (!File.Exists(destinationFile))
            {
                File.Copy(sourceFile, destinationFile);
                Program.Wallpaper = (Bitmap)Image.FromFile(destinationFile);
            }
            return destinationFile;
        }

        /// <summary>
        /// Если окно реализует интерфейс <see cref="IHaveWallpaper"/>, то обновить обои.
        /// </summary>
        /// <param name="form">Окно</param>
        private static void UpdateWallpaperInternal(Form form)
        {
            if (Wallpaper != null && form is IHaveWallpaper)
            {
                ((IHaveWallpaper)form).UpdateWallpaper(Wallpaper);
            }
        }

    }
}
