﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using BHOSV002BETA;
using WinFormsApplication;
using WMPLib;

namespace BHOSV002BETA
{
    public partial class MainForm : Form, IHaveWallpaper
    {
        private bool _userClosingReason = true;

        public MainForm(Form form)
        {
            InitializeComponent();
            Debug.Assert(form != null, "в конструктор окна передан параметр form как null");
            Owner = form;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dtlablel.Text = DateTime.Now.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (ConfirmApplicationClosing())
            {
                _userClosingReason = false;
                Application.Exit();
            }
        }

        private static bool ConfirmApplicationClosing()
        {
            var mb = MessageBox.Show("Выход из программы, вы уверены?", "Завершение программы!", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            return mb == DialogResult.Yes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool newState = !pMenu.Visible;

            pMenu.Visible = newState;
            button2.Visible = newState;
            button5.Visible = newState;
            settingsb.Visible = newState;
            islogin.Visible = newState;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ProgMenu.Visible == !true)
            {
                ProgMenu.Visible = true;
                calc.Visible = true;
                Browser.Visible = true;
            }
            else
            {
                ProgMenu.Visible = false;
                calc.Visible = false;
                Browser.Visible = false;
            }
        }

        private void calc_Click(object sender, EventArgs e)
        {
            var c = new CalcForm();
            c.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var wb = new WebBrowser();
            wb.Show();
        }

        private void islogin_Click(object sender, EventArgs e)
        {
            _userClosingReason = false;
            Owner.Show();
            Close();
        }

        private void settingsb_Click(object sender, EventArgs e)
        {
            Hide();
            var sf = new FormCP(this);
            Program.ShowForm(sf);
        }

        #region Implementation of IHaveWallpaper

        public void UpdateWallpaper(Image image)
        {
            if (desktopimg != null)
            {
                desktopimg.Image = image;
            }
        }

        #endregion

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_userClosingReason)
            {
                if (ConfirmApplicationClosing())
                {
                    _userClosingReason = false;
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void MainForm_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                _userClosingReason = true;
            }
        }

        private void npb_Click(object sender, EventArgs e)
        {
            var np = new Notepad();
            np.Show();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer");
        }
    }
}