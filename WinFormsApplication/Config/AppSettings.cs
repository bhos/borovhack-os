﻿using System;

namespace WinFormsApplication.Config
{
    /// <summary>
    /// Класс, представляющий из себя настройки приложения
    /// </summary>
    [Serializable]
    public class AppSettings
    {
        public AppSettings()
        {
        }

        /// <summary>
        /// Полный путь к файлу обоев для рабочего стола приложения
        /// </summary>
        public string WallpaperFile { get; set; }
    }
}
