﻿using System;
using System.Windows.Forms;

namespace BHOSV002BETA
{
    public partial class WebBrowser : Form
    {
        public WebBrowser()
        {
            
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("http://" + textBox1.Text);
        }
    }
}
