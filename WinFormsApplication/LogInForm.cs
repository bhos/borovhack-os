﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using WinFormsApplication.Config;

namespace BHOSV002BETA
{
    public partial class LogInForm : Form
    {
        private readonly string defPath = AppContext.Instance.UsersFile;

        public LogInForm()
        {
            InitializeComponent();
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            Program.InitializeConfiguration();
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            Hide();
            var register = new RegForm(this); //передаём ссылку на это окно
            Program.ShowForm(register);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool ReadXMLDocument(string filepath, string loget, string passget)
        {
            if (!File.Exists(filepath))
                return false;
            
            var xd = new XmlDocument();
            using (var fs = new FileStream(filepath, FileMode.Open))
            {
                xd.Load(fs);
            }
            XmlNodeList list = xd.GetElementsByTagName("user");
            for (var i = 0; i < list.Count; i++)
            {
                var user = (XmlElement)xd.GetElementsByTagName("login")[i];
                var pass = (XmlElement)xd.GetElementsByTagName("password")[i];

                if ((loget == user.InnerText) & (passget == pass.InnerText))
                {
                    return true;
                }

                if (i == list.Count - 1)
                    return false;
            }

            return false;
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(sLogIn.Text) || string.IsNullOrWhiteSpace(sPassword.Text))
            {
                MessageBox.Show(this, "Введите логин и пароль");
            }
            else
            {
                Login();
            }
        }

        private void Login()
        {
            //&& - если первое условие == null, то второе проверяться не будет (да и смысла его уже проверять нет)
            //при использовании & проверяться будут в любом случае 2 условия, но в данном случае это не имеет смысла
            if (sLogIn.Text != "" && sPassword.Text != "")
            {
                if (ReadXMLDocument(defPath, sLogIn.Text, sPassword.Text))
                {
                    Hide();
                    var ld = new MainForm(this); //передаём ссылку на это окно
                    Program.ShowForm(ld);
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var rp = new resetpass();
            Program.ShowForm(rp);

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}