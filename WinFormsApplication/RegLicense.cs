﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace BHOSV002BETA
{
    public partial class RegLicense : Form
    {
        public RegLicense(Form form)
        {
            InitializeComponent();
            Debug.Assert(form != null, "в конструктор окна передан параметр form как null");
            Owner = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            Owner.Show();
        }

        private void RegLicense_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner.Show();
        }
    }
}
