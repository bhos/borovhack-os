﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using WinFormsApplication;
using WinFormsApplication.Config;

namespace BHOSV002BETA
{
    public partial class FormCP : Form, IHaveWallpaper
    {
        public FormCP(Form form)
        {
            InitializeComponent();
            Debug.Assert(form != null, "в конструктор окна передан параметр form как null");
            Owner = form;
        }

        private void FormCP_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.ShowForm(Owner);
        }

        public void choise_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string file = Program.SetNewWallpaper(openFileDialog1.FileName);
                LocalConfig.Instance.Settings.WallpaperFile = file;
                UpdateWallpaper(Program.Wallpaper);
                LocalConfig.Instance.SaveCurrentSettings();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ShowForm(Owner);
            Close();
        }

        private void SaveImg_Click(object sender, EventArgs e)
        {
        }

        private void standart_Click(object sender, EventArgs e)
        {
        }

        //public class Settings
        //{
        //    // Конструктор класса
        //    public Settings(string deskimg)
        //    {
        //        var ofd = new OpenFileDialog();
        //        ofd.FileName = deskimg;
        //    }

        //    // Свойство Email
        //    public String deskimg { get; set; }
        //}

        //public class SettingsManager
        //{
        //    private readonly string fileName = "DeskImg.conf";

        //    public void Save(Settings settings)
        //    {
        //        try
        //        {
        //            using (var sw = new StreamWriter(File.Open(fileName, FileMode.Create)))
        //            {
        //                sw.WriteLine(settings.deskimg);
        //                sw.Close();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //    }
        //}

        #region Implementation of IHaveWallpaper

        public void UpdateWallpaper(Image image)
        {
            GroupBox gb = new GroupBox();
            if (pictureBox1 != null)
            {
                pictureBox1.Image = image;
            }

            if (gb.BackgroundImage != null)
            {
                gb.BackgroundImage = image;
            }
        }

        #endregion

        private void FormCP_Load(object sender, EventArgs e)
        {

        }
    }
}