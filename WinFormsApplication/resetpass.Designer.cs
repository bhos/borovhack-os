﻿namespace BHOSV002BETA
{
    partial class resetpass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TB1 = new System.Windows.Forms.TextBox();
            this.TB2 = new System.Windows.Forms.TextBox();
            this.TB3 = new System.Windows.Forms.TextBox();
            this.TB4 = new System.Windows.Forms.TextBox();
            this.TB5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TB6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.resetp = new System.Windows.Forms.Button();
            this.TB7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.l4 = new System.Windows.Forms.Label();
            this.l5 = new System.Windows.Forms.Label();
            this.l6 = new System.Windows.Forms.Label();
            this.l7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.BurlyWood;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(491, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Данная форма восстановления пароля предназначена для тех,\r\nкто забыл, потерял, лю" +
    "дей которых взломали.";
            // 
            // TB1
            // 
            this.TB1.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB1.ForeColor = System.Drawing.Color.Coral;
            this.TB1.Location = new System.Drawing.Point(126, 57);
            this.TB1.Name = "TB1";
            this.TB1.Size = new System.Drawing.Size(171, 20);
            this.TB1.TabIndex = 1;
            // 
            // TB2
            // 
            this.TB2.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB2.ForeColor = System.Drawing.Color.Coral;
            this.TB2.Location = new System.Drawing.Point(126, 83);
            this.TB2.Name = "TB2";
            this.TB2.Size = new System.Drawing.Size(171, 20);
            this.TB2.TabIndex = 2;
            // 
            // TB3
            // 
            this.TB3.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB3.ForeColor = System.Drawing.Color.Coral;
            this.TB3.Location = new System.Drawing.Point(126, 111);
            this.TB3.Name = "TB3";
            this.TB3.Size = new System.Drawing.Size(171, 20);
            this.TB3.TabIndex = 3;
            // 
            // TB4
            // 
            this.TB4.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB4.ForeColor = System.Drawing.Color.Coral;
            this.TB4.Location = new System.Drawing.Point(126, 135);
            this.TB4.Name = "TB4";
            this.TB4.Size = new System.Drawing.Size(171, 20);
            this.TB4.TabIndex = 4;
            // 
            // TB5
            // 
            this.TB5.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB5.ForeColor = System.Drawing.Color.Coral;
            this.TB5.Location = new System.Drawing.Point(126, 161);
            this.TB5.Name = "TB5";
            this.TB5.Size = new System.Drawing.Size(171, 20);
            this.TB5.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.BurlyWood;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(30, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Логин";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.BurlyWood;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(30, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Имя ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.BurlyWood;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(30, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Фамилия";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.BurlyWood;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(30, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "E-Mail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.BurlyWood;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(30, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 19);
            this.label6.TabIndex = 10;
            this.label6.Text = "Город";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.BurlyWood;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(30, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 19);
            this.label7.TabIndex = 12;
            this.label7.Text = "Проверка";
            // 
            // TB6
            // 
            this.TB6.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB6.ForeColor = System.Drawing.Color.Coral;
            this.TB6.Location = new System.Drawing.Point(126, 194);
            this.TB6.Name = "TB6";
            this.TB6.Size = new System.Drawing.Size(171, 20);
            this.TB6.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.BurlyWood;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(303, 191);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 19);
            this.label8.TabIndex = 13;
            this.label8.Text = "BHOS203";
            // 
            // resetp
            // 
            this.resetp.BackColor = System.Drawing.SystemColors.ControlText;
            this.resetp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetp.ForeColor = System.Drawing.Color.Coral;
            this.resetp.Location = new System.Drawing.Point(466, 269);
            this.resetp.Name = "resetp";
            this.resetp.Size = new System.Drawing.Size(93, 23);
            this.resetp.TabIndex = 14;
            this.resetp.Text = "Сбросить";
            this.resetp.UseVisualStyleBackColor = false;
            this.resetp.Click += new System.EventHandler(this.resetp_Click);
            // 
            // TB7
            // 
            this.TB7.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB7.ForeColor = System.Drawing.Color.Coral;
            this.TB7.Location = new System.Drawing.Point(34, 246);
            this.TB7.Name = "TB7";
            this.TB7.Size = new System.Drawing.Size(273, 20);
            this.TB7.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.BurlyWood;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(30, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(277, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "Кодовое слово(при регистрации)";
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Location = new System.Drawing.Point(303, 62);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(0, 13);
            this.l1.TabIndex = 17;
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Location = new System.Drawing.Point(303, 89);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(0, 13);
            this.l2.TabIndex = 18;
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.Location = new System.Drawing.Point(303, 112);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(0, 13);
            this.l3.TabIndex = 19;
            // 
            // l4
            // 
            this.l4.AutoSize = true;
            this.l4.Location = new System.Drawing.Point(303, 139);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(0, 13);
            this.l4.TabIndex = 19;
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.Location = new System.Drawing.Point(303, 165);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(0, 13);
            this.l5.TabIndex = 19;
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.Location = new System.Drawing.Point(392, 194);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(0, 13);
            this.l6.TabIndex = 19;
            // 
            // l7
            // 
            this.l7.AutoSize = true;
            this.l7.Location = new System.Drawing.Point(313, 250);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(0, 13);
            this.l7.TabIndex = 19;
            // 
            // resetpass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(571, 295);
            this.Controls.Add(this.l7);
            this.Controls.Add(this.l6);
            this.Controls.Add(this.l5);
            this.Controls.Add(this.l4);
            this.Controls.Add(this.l3);
            this.Controls.Add(this.l2);
            this.Controls.Add(this.l1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TB7);
            this.Controls.Add(this.resetp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TB6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TB5);
            this.Controls.Add(this.TB4);
            this.Controls.Add(this.TB3);
            this.Controls.Add(this.TB2);
            this.Controls.Add(this.TB1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "resetpass";
            this.Text = "Восстановление доступа";
            this.Load += new System.EventHandler(this.resetpass_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB1;
        private System.Windows.Forms.TextBox TB2;
        private System.Windows.Forms.TextBox TB3;
        private System.Windows.Forms.TextBox TB4;
        private System.Windows.Forms.TextBox TB5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TB6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button resetp;
        private System.Windows.Forms.TextBox TB7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Label l4;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label l6;
        private System.Windows.Forms.Label l7;
    }
}