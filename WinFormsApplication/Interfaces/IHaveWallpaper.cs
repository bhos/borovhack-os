﻿using System.Drawing;

namespace WinFormsApplication
{
    public interface IHaveWallpaper
    {
        void UpdateWallpaper(Image image);
    }
}
