using System;
using System.Collections.Generic;
using System.IO;

namespace WinFormsApplication.Config
{
    /// <summary>
    /// �����, �������� � ���� �������� ����� � ������ � ������ ����������
    /// </summary>
    public class AppContext
    {
        //���������, ����� ����� ���� ������� ��� ��������� ����� � ���� ����-��������.
        //����� ����� ���� ����������� �� ���� ��������� ������, �, ��������, ��������� �����,
        //���� ��� �� ����������. 
        private readonly Dictionary<Folder, string> _folders;

        static AppContext()
        {
            Instance = new AppContext();
        }

        /// <summary>
        /// �����������. �������������� ���� � ������ � ������ ����������.
        /// </summary>
        private AppContext()
        {
            //���� � ������
            string appDataFolder = Path.Combine(
                 Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Const.FolderName.Root); //..DTO
            string wallpaperCacheFolder = Path.Combine(appDataFolder, Const.FolderName.WallpaperCache); //-----//..DTO/WallpaperCache

            //���� � ������
            SettingsFile = Path.Combine(appDataFolder, Const.FileName.SettingsFileName); //..DTO/Settings.xml
            UsersFile = Path.Combine(appDataFolder, Const.FileName.UsersFileName); //----//..DTO/Users.xml

            _folders = new Dictionary<Folder, string>();
            _folders.Add(Folder.AppData, appDataFolder);
            _folders.Add(Folder.WallpaperCache, wallpaperCacheFolder);
        }

        /// <summary>
        /// �������� ���� � ����� ���������� �� �������. � �������� ������� ��������� 
        /// ������������ � ������� �����. ������ �������������: 
        /// <code>string text = AppContext.Instance.GetPath(Folder.AppData);</code>
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetPath(Folder index)
        {
            return _folders[index];
        }

        #region ��������

        /// <summary>
        /// ������������ ��������� ������� � ������
        /// </summary>
        public static AppContext Instance { get; private set; }

        /// <summary>
        /// ������ ���� � ����� ��������
        /// </summary>
        public string SettingsFile { get; private set; }

        /// <summary>
        /// ������ ���� � ����� �� ������� �������������
        /// </summary>
        public string UsersFile { get; private set; }

        #endregion

        //----------------------------------------------------

        /// <summary>
        /// ��������� ����� �� �������������, ���� �� ����������, �� ������� ��
        /// </summary>
        public void CheckFolders()
        {
            //������ �� ���� ����� � ������� ��� ��� �����, ���� �� ����������
            foreach (KeyValuePair<Folder, string> pair in _folders)
            {
                if (!Directory.Exists(pair.Value))
                    Directory.CreateDirectory(pair.Value);
            }
        }
    }
}