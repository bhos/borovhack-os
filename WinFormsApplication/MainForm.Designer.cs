﻿namespace BHOSV002BETA
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.desktopimg = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtlablel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pMenu = new System.Windows.Forms.Panel();
            this.settingsb = new System.Windows.Forms.Button();
            this.islogin = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ProgMenu = new System.Windows.Forms.Panel();
            this.npb = new System.Windows.Forms.Button();
            this.Browser = new System.Windows.Forms.Button();
            this.calc = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.desktopimg)).BeginInit();
            this.panel1.SuspendLayout();
            this.pMenu.SuspendLayout();
            this.ProgMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // desktopimg
            // 
            resources.ApplyResources(this.desktopimg, "desktopimg");
            this.desktopimg.Name = "desktopimg";
            this.desktopimg.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.dtlablel);
            this.panel1.Controls.Add(this.button1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtlablel
            // 
            resources.ApplyResources(this.dtlablel, "dtlablel");
            this.dtlablel.BackColor = System.Drawing.SystemColors.WindowText;
            this.dtlablel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtlablel.ForeColor = System.Drawing.Color.Coral;
            this.dtlablel.Name = "dtlablel";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.ForeColor = System.Drawing.Color.Coral;
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.button5, "button5");
            this.button5.ForeColor = System.Drawing.Color.Coral;
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // pMenu
            // 
            this.pMenu.BackColor = System.Drawing.Color.Black;
            this.pMenu.Controls.Add(this.settingsb);
            this.pMenu.Controls.Add(this.islogin);
            this.pMenu.Controls.Add(this.button2);
            this.pMenu.Controls.Add(this.button5);
            resources.ApplyResources(this.pMenu, "pMenu");
            this.pMenu.Name = "pMenu";
            // 
            // settingsb
            // 
            this.settingsb.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.settingsb, "settingsb");
            this.settingsb.ForeColor = System.Drawing.Color.Coral;
            this.settingsb.Name = "settingsb";
            this.settingsb.UseVisualStyleBackColor = false;
            this.settingsb.Click += new System.EventHandler(this.settingsb_Click);
            // 
            // islogin
            // 
            this.islogin.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.islogin, "islogin");
            this.islogin.ForeColor = System.Drawing.Color.Coral;
            this.islogin.Name = "islogin";
            this.islogin.UseVisualStyleBackColor = false;
            this.islogin.Click += new System.EventHandler(this.islogin_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.button2, "button2");
            this.button2.ForeColor = System.Drawing.Color.Coral;
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ProgMenu
            // 
            this.ProgMenu.BackColor = System.Drawing.Color.Black;
            this.ProgMenu.Controls.Add(this.npb);
            this.ProgMenu.Controls.Add(this.Browser);
            this.ProgMenu.Controls.Add(this.calc);
            resources.ApplyResources(this.ProgMenu, "ProgMenu");
            this.ProgMenu.Name = "ProgMenu";
            // 
            // npb
            // 
            this.npb.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.npb, "npb");
            this.npb.ForeColor = System.Drawing.Color.Coral;
            this.npb.Name = "npb";
            this.npb.UseVisualStyleBackColor = false;
            this.npb.Click += new System.EventHandler(this.npb_Click);
            // 
            // Browser
            // 
            this.Browser.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.Browser, "Browser");
            this.Browser.ForeColor = System.Drawing.Color.Coral;
            this.Browser.Name = "Browser";
            this.Browser.UseVisualStyleBackColor = false;
            this.Browser.Click += new System.EventHandler(this.button3_Click);
            // 
            // calc
            // 
            this.calc.BackColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.calc, "calc");
            this.calc.ForeColor = System.Drawing.Color.Coral;
            this.calc.Name = "calc";
            this.calc.UseVisualStyleBackColor = false;
            this.calc.Click += new System.EventHandler(this.calc_Click);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ProgMenu);
            this.Controls.Add(this.pMenu);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.desktopimg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.MainForm_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.desktopimg)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pMenu.ResumeLayout(false);
            this.ProgMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        public System.Windows.Forms.Label dtlablel;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel pMenu;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel ProgMenu;
        private System.Windows.Forms.Button calc;
        private System.Windows.Forms.Button Browser;
        private System.Windows.Forms.Button islogin;
        private System.Windows.Forms.Button settingsb;
        public System.Windows.Forms.PictureBox desktopimg;
        private System.Windows.Forms.Button npb;
    }
}