﻿namespace BHOSV002BETA
{
    partial class FormCP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCP));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Desktop = new System.Windows.Forms.GroupBox();
            this.choise = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Desktop.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(906, 736);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Desktop
            // 
            this.Desktop.BackColor = System.Drawing.SystemColors.Desktop;
            this.Desktop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Desktop.BackgroundImage")));
            this.Desktop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Desktop.Controls.Add(this.choise);
            this.Desktop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Desktop.Location = new System.Drawing.Point(-1, -3);
            this.Desktop.Name = "Desktop";
            this.Desktop.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Desktop.Size = new System.Drawing.Size(906, 736);
            this.Desktop.TabIndex = 1;
            this.Desktop.TabStop = false;
            this.Desktop.Text = "Картинка рабочего стола";
            // 
            // choise
            // 
            this.choise.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.choise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.choise.ForeColor = System.Drawing.Color.Coral;
            this.choise.Location = new System.Drawing.Point(13, 694);
            this.choise.Name = "choise";
            this.choise.Size = new System.Drawing.Size(456, 23);
            this.choise.TabIndex = 0;
            this.choise.Text = "Выбрать";
            this.choise.UseVisualStyleBackColor = false;
            this.choise.Click += new System.EventHandler(this.choise_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Coral;
            this.button1.Location = new System.Drawing.Point(474, 691);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(412, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Рабочий стол";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormCP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 726);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Desktop);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Панель управления";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCP_FormClosing);
            this.Load += new System.EventHandler(this.FormCP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Desktop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox Desktop;
        private System.Windows.Forms.Button choise;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
    }
}