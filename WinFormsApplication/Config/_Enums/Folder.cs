namespace WinFormsApplication.Config
{
    /// <summary>
    /// �������� � ���� ������������� �������� ��� ����������� �����
    /// </summary>
    public enum Folder
    {
        /// <summary>
        /// ����� ��� �������� ������ ����������
        /// </summary>
        AppData,

        /// <summary>
        /// ����� ��� ����������� ����� ����������
        /// </summary>
        WallpaperCache
    }
}