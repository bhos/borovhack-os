﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using WinFormsApplication;
using WinFormsApplication.Config;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace BHOSV002BETA
{
    public partial class RegForm : Form, IHaveWallpaper
    {
        private readonly string defPath = AppContext.Instance.UsersFile;

        public RegForm(Form form)
        {
            InitializeComponent();
            Debug.Assert(form != null, "в конструктор окна передан параметр form как null");
            Owner = form;
        }

        private void CreateXMLDocument(string filepath) //создание xml файла
        {
            var xtw = new XmlTextWriter(filepath, Encoding.UTF32);
            xtw.WriteStartDocument();
            xtw.WriteStartElement("users");
            xtw.WriteEndDocument();
            xtw.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            var license = new RegLicense(this);
            license.ShowDialog();
        }

            private void insertData()
        {

            /*Host: sql2.freemysqlhosting.net
            Database name: sql291764
            Database user: sql291764
            Database password: cR2!dR4*
            Port number: 3306*/
            var g = birthd.SelectedItem + "." + birthm.SelectedItem + "." + birthy.Text;
            string conStr = "server=sql2.freesqldatabase.com;user=sql291764;" +
                "database=sql291764;password=cR2!dR4*";
            using (MySqlConnection con = new MySqlConnection(conStr))
            {
                try
                {
                    string sql = "INSERT INTO `users` (login, name, sname, email, password, city, keyword, born)" +
                     "VALUES ('" + TB1.Text + "', '" + TB2.Text + "', '" + TB3.Text + "', '" + TB4.Text + "', '" + TB6.Text + "', '" + TB5.Text + "', '" + TB7.Text + "', '" + g + "' )";
                    MySqlCommand cmd = new MySqlCommand(sql, con);

                    con.Open();

                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Данные добавлены!");
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void regnext_Click(object sender, EventArgs e)
        {
            pb1.Image.Save(TB1.Text + ".jpg");
            insertData();

            var g = birthd.SelectedItem + "." + birthm.SelectedItem + "." + birthy.Text;

            StreamWriter write_text; //Класс для записи в файл
            var file2 = new FileInfo(TB1.Text + ".txt");
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Login:" + TB1.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Name:" + TB2.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("SurName:" + TB3.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Mail:" + TB4.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("City:" + TB5.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Password:" + TB6.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Дата Рождения: " + g);
            write_text.Close();// Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("Кодовое слово: " + TB7.Text);
            write_text.Close(); // Закрываем файл
            write_text = file2.AppendText(); //Дописываем инфу в файл, если файла не существует он создастся
            write_text.WriteLine("---------------------------");
            write_text.Close();

            var fileName = TB1.Text + ".txt";
            var ftpServerIP = "31.220.16.128";
            var ftpUserID = "u314821889";
            var ftpPassword = "139713";
            var fileInf = new FileInfo(fileName);
            var uri = "ftp://" + ftpServerIP + fileInf.Name;
            FtpWebRequest reqFTP;
            reqFTP = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + ftpServerIP + "/" + fileInf.Name));
            reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
            reqFTP.KeepAlive = false;
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
            reqFTP.UseBinary = true;
            reqFTP.ContentLength = fileInf.Length;
            var buffLength = 2048;
            var buff = new byte[buffLength];
            int contentLen;
            var fs = fileInf.OpenRead();
            try
            {
                var strm = reqFTP.GetRequestStream();
                contentLen = fs.Read(buff, 0, buffLength);
                while (contentLen != 0)
                {
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }
                strm.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Upload Error");
            }

            if (TB1.Text != "" & TB6.Text != "")
            {
                WriteToXMLDocument(defPath, TB1.Text, TB6.Text);
                Owner.Show();
                Close();
            }
            else MessageBox.Show("Введите имя пользователя и пароль");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            regnext.Enabled = checkBox1.Checked;
        }

        private string MaxID(string filepath) //вычисление max id
        {
            var idList = new List<int>();
            var xd = new XmlDocument();
            using (var fs = new FileStream(filepath, FileMode.Open))
            {
                xd.Load(fs);
            }
            var list = xd.GetElementsByTagName("user");
            if (list.Count > 0)
            {
                for (var i = 0; i < list.Count; i++)
                {
                    var user = (XmlElement)xd.GetElementsByTagName("user")[i];
                    int id = Convert.ToInt32(user.GetAttribute("id"));
                    idList.Add(id);
                }
                int result = 0;
                foreach (var j in idList)
                    if (j > result)
                        result = j;
                result++;
                return result.ToString();
            }
            return "1";
        }

        private void WriteToXMLDocument(string filepath, string name, string pwd) //добавление записи
        {
            if (!File.Exists(defPath)) CreateXMLDocument(defPath);
            var id = MaxID(filepath);
            var xd = new XmlDocument();
            using (var fs = new FileStream(filepath, FileMode.Open))
            {
                xd.Load(fs);

                var user = xd.CreateElement("user");
                user.SetAttribute("id", id);

                var login = xd.CreateElement("login");
                var pass = xd.CreateElement("password");

                var tLogin = xd.CreateTextNode(name);
                var tPassword = xd.CreateTextNode(pwd);

                login.AppendChild(tLogin);
                pass.AppendChild(tPassword);

                user.AppendChild(login);
                user.AppendChild(pass);

                if (xd.DocumentElement != null)
                {
                    xd.DocumentElement.AppendChild(user);
                }
                else
                {
                    Debug.Fail("xd.DocumentElement == null");
                }
            }
            xd.Save(filepath);
        }


        private void RegForm_Load(object sender, EventArgs e)
        {
            if (!File.Exists(defPath)) CreateXMLDocument(defPath);
            MessageBox.Show("Уважаемый пользователь,во избежания неисправности приложения, убедительная просьба, все данные вводить английскими символами,и цифрами,иначе мы не обещаем работоспособность приложения", "Внимание!", MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
        private void RegForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner.Show();
        }

        #region Implementation of IHaveWallpaper

        public void UpdateWallpaper(Image image)
        {
            if (pb1 != null)
            {
                pb1.Image = image;
            }
        }

        #endregion

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            PictureBox pb11 = new PictureBox();
            if (opd.ShowDialog() == DialogResult.OK)
            {
               pb1.Image = Image.FromFile(opd.FileName);
               pb1.Image.Save("%appdata%");
            }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            if (opd.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(opd.FileName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}