﻿namespace BHOSV002BETA
{
    partial class RegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegForm));
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.TB6 = new System.Windows.Forms.TextBox();
            this.TB5 = new System.Windows.Forms.TextBox();
            this.TB4 = new System.Windows.Forms.TextBox();
            this.TB3 = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.regnext = new System.Windows.Forms.Button();
            this.fname = new System.Windows.Forms.Label();
            this.LName = new System.Windows.Forms.Label();
            this.mail = new System.Windows.Forms.Label();
            this.City = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.birthm = new System.Windows.Forms.ComboBox();
            this.BirthDay = new System.Windows.Forms.Label();
            this.birthd = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.TB1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TB7 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TB2 = new System.Windows.Forms.TextBox();
            this.birthy = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.opd = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pb1
            // 
            this.pb1.Image = ((System.Drawing.Image)(resources.GetObject("pb1.Image")));
            this.pb1.Location = new System.Drawing.Point(-6, -2);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(856, 710);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb1.TabIndex = 4;
            this.pb1.TabStop = false;
            this.pb1.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // TB6
            // 
            this.TB6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB6.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB6.Location = new System.Drawing.Point(309, 531);
            this.TB6.Name = "TB6";
            this.TB6.PasswordChar = '*';
            this.TB6.Size = new System.Drawing.Size(217, 20);
            this.TB6.TabIndex = 6;
            // 
            // TB5
            // 
            this.TB5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB5.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB5.Location = new System.Drawing.Point(309, 507);
            this.TB5.Name = "TB5";
            this.TB5.Size = new System.Drawing.Size(217, 20);
            this.TB5.TabIndex = 5;
            // 
            // TB4
            // 
            this.TB4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB4.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB4.Location = new System.Drawing.Point(309, 479);
            this.TB4.Name = "TB4";
            this.TB4.Size = new System.Drawing.Size(217, 20);
            this.TB4.TabIndex = 4;
            // 
            // TB3
            // 
            this.TB3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB3.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB3.Location = new System.Drawing.Point(309, 453);
            this.TB3.Name = "TB3";
            this.TB3.Size = new System.Drawing.Size(217, 20);
            this.TB3.TabIndex = 3;
            // 
            // linkLabel1
            // 
            this.linkLabel1.BackColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.linkLabel1.Location = new System.Drawing.Point(306, 653);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(212, 35);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Лицензионным соглашением правил использования приложения";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // regnext
            // 
            this.regnext.BackColor = System.Drawing.SystemColors.ControlText;
            this.regnext.Enabled = false;
            this.regnext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regnext.ForeColor = System.Drawing.Color.Coral;
            this.regnext.Location = new System.Drawing.Point(700, 666);
            this.regnext.Name = "regnext";
            this.regnext.Size = new System.Drawing.Size(125, 26);
            this.regnext.TabIndex = 12;
            this.regnext.Text = "Далее";
            this.regnext.UseVisualStyleBackColor = false;
            this.regnext.Click += new System.EventHandler(this.regnext_Click);
            // 
            // fname
            // 
            this.fname.AutoSize = true;
            this.fname.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.fname.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.fname.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.fname.Location = new System.Drawing.Point(261, 428);
            this.fname.Name = "fname";
            this.fname.Size = new System.Drawing.Size(42, 19);
            this.fname.TabIndex = 13;
            this.fname.Text = "Имя";
            // 
            // LName
            // 
            this.LName.AutoSize = true;
            this.LName.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LName.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.LName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.LName.Location = new System.Drawing.Point(221, 454);
            this.LName.Name = "LName";
            this.LName.Size = new System.Drawing.Size(82, 19);
            this.LName.TabIndex = 14;
            this.LName.Text = "Фамилия";
            // 
            // mail
            // 
            this.mail.AutoSize = true;
            this.mail.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.mail.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.mail.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.mail.Location = new System.Drawing.Point(248, 480);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(56, 19);
            this.mail.TabIndex = 15;
            this.mail.Text = "E-Mail";
            // 
            // City
            // 
            this.City.AutoSize = true;
            this.City.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.City.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.City.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.City.Location = new System.Drawing.Point(248, 506);
            this.City.Name = "City";
            this.City.Size = new System.Drawing.Size(54, 19);
            this.City.TabIndex = 16;
            this.City.Text = "Город";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.password.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.password.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.password.Location = new System.Drawing.Point(236, 532);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(67, 19);
            this.password.TabIndex = 17;
            this.password.Text = "Пароль";
            // 
            // birthm
            // 
            this.birthm.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.birthm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthm.ForeColor = System.Drawing.Color.Coral;
            this.birthm.FormattingEnabled = true;
            this.birthm.Items.AddRange(new object[] {
            "January\t",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "September",
            "October",
            "November",
            "December"});
            this.birthm.Location = new System.Drawing.Point(354, 585);
            this.birthm.Name = "birthm";
            this.birthm.Size = new System.Drawing.Size(108, 21);
            this.birthm.TabIndex = 9;
            // 
            // BirthDay
            // 
            this.BirthDay.AutoSize = true;
            this.BirthDay.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BirthDay.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.BirthDay.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.BirthDay.Location = new System.Drawing.Point(169, 587);
            this.BirthDay.Name = "BirthDay";
            this.BirthDay.Size = new System.Drawing.Size(129, 19);
            this.BirthDay.TabIndex = 21;
            this.BirthDay.Text = "Дата рождения";
            // 
            // birthd
            // 
            this.birthd.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.birthd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthd.ForeColor = System.Drawing.Color.Coral;
            this.birthd.FormattingEnabled = true;
            this.birthd.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.birthd.Location = new System.Drawing.Point(307, 585);
            this.birthd.Name = "birthd";
            this.birthd.Size = new System.Drawing.Size(44, 21);
            this.birthd.TabIndex = 8;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.SystemColors.ControlText;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.ForeColor = System.Drawing.Color.YellowGreen;
            this.checkBox1.Location = new System.Drawing.Point(306, 633);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(209, 17);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "Я полностью прочитал и согласен с ";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // TB1
            // 
            this.TB1.BackColor = System.Drawing.SystemColors.ControlText;
            this.TB1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB1.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB1.Location = new System.Drawing.Point(309, 401);
            this.TB1.Name = "TB1";
            this.TB1.Size = new System.Drawing.Size(217, 20);
            this.TB1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(248, 400);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 19);
            this.label1.TabIndex = 25;
            this.label1.Text = "Логин";
            // 
            // TB7
            // 
            this.TB7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB7.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB7.Location = new System.Drawing.Point(309, 557);
            this.TB7.Name = "TB7";
            this.TB7.PasswordChar = '*';
            this.TB7.Size = new System.Drawing.Size(217, 20);
            this.TB7.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(180, 558);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 19);
            this.label2.TabIndex = 27;
            this.label2.Text = "Кодовое слово";
            // 
            // TB2
            // 
            this.TB2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TB2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TB2.ForeColor = System.Drawing.Color.YellowGreen;
            this.TB2.Location = new System.Drawing.Point(309, 427);
            this.TB2.Name = "TB2";
            this.TB2.Size = new System.Drawing.Size(217, 20);
            this.TB2.TabIndex = 2;
            this.TB2.Text = " ";
            // 
            // birthy
            // 
            this.birthy.BackColor = System.Drawing.SystemColors.WindowText;
            this.birthy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.birthy.ForeColor = System.Drawing.Color.Coral;
            this.birthy.FormattingEnabled = true;
            this.birthy.Items.AddRange(new object[] {
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015"});
            this.birthy.Location = new System.Drawing.Point(468, 585);
            this.birthy.Name = "birthy";
            this.birthy.Size = new System.Drawing.Size(58, 21);
            this.birthy.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlText;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Coral;
            this.button1.Location = new System.Drawing.Point(12, 666);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 26);
            this.button1.TabIndex = 13;
            this.button1.Text = "Назад";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // opd
            // 
            this.opd.FileName = "opd";
            this.opd.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(354, 258);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // RegForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 704);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.birthy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TB7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB1);
            this.Controls.Add(this.birthd);
            this.Controls.Add(this.BirthDay);
            this.Controls.Add(this.birthm);
            this.Controls.Add(this.password);
            this.Controls.Add(this.City);
            this.Controls.Add(this.mail);
            this.Controls.Add(this.LName);
            this.Controls.Add(this.fname);
            this.Controls.Add(this.regnext);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.TB3);
            this.Controls.Add(this.TB4);
            this.Controls.Add(this.TB5);
            this.Controls.Add(this.TB2);
            this.Controls.Add(this.TB6);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.pb1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "RegForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Форма регистрации в BHOS v 0.0.2b";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegForm_FormClosing);
            this.Load += new System.EventHandler(this.RegForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label fname;
        private System.Windows.Forms.Label LName;
        private System.Windows.Forms.Label mail;
        private System.Windows.Forms.Label City;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.ComboBox birthm;
        private System.Windows.Forms.Label BirthDay;
        private System.Windows.Forms.ComboBox birthd;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox TB6;
        public System.Windows.Forms.TextBox TB5;
        public System.Windows.Forms.TextBox TB4;
        public System.Windows.Forms.TextBox TB3;
        public System.Windows.Forms.TextBox TB1;
        public System.Windows.Forms.Button regnext;
        public System.Windows.Forms.TextBox TB7;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox TB2;
        private System.Windows.Forms.ComboBox birthy;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.OpenFileDialog opd;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}