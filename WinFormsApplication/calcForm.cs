﻿using System;
using System.Windows.Forms;

namespace BHOSV002BETA
{
    public partial class CalcForm : Form
    {
        private bool delit;
        private bool minus;
        private bool plus;
        private bool umnozit;

        public CalcForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "9";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Tag = "";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            plus = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            minus = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            umnozit = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            delit = true;
            textBox1.Tag = textBox1.Text;
            textBox1.Text = "";
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                double x;
                x = Convert.ToDouble(textBox1.Text)*Convert.ToDouble(textBox1.Text);
                textBox1.Text = Convert.ToString(x);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                double k;
                k = Convert.ToDouble(textBox1.Text);
                textBox1.Text = Convert.ToString(Math.Sqrt(k));
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (plus)
            {
                var pl = Convert.ToDecimal(textBox1.Tag) + Convert.ToDecimal(textBox1.Text);
                textBox1.Text = pl.ToString();
            }

            if (minus)
            {
                var pl = Convert.ToDecimal(textBox1.Tag) - Convert.ToDecimal(textBox1.Text);
                textBox1.Text = pl.ToString();
            }

            if (umnozit)
            {
                var pl = Convert.ToDecimal(textBox1.Tag)*
                         Convert.ToDecimal(textBox1.Text);
                textBox1.Text = pl.ToString();
            }

            if (delit)
            {
                var pl = Convert.ToDecimal(textBox1.Tag)/
                         Convert.ToDecimal(textBox1.Text);
                textBox1.Text = pl.ToString();
            }
        }

        private void CalcForm_Load(object sender, EventArgs e)
        {

        }
    }
}