﻿using System;
using System.Windows.Forms;

namespace WinFormsApplication
{
    public partial class Notepad : Form
    {
        public Notepad()
        {
            InitializeComponent();
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var new1 = new TabPage("new " + (tabControl1.Controls.Count + 1));
            var tb = new RichTextBox();
            tb.ContextMenuStrip = contextMenuStrip1;
            tb.Dock = DockStyle.Fill;
            tabControl1.TabPages.Add(new1);
            new1.Controls.Add(tb);
            tabControl1.SelectedTab = new1;
        }

        private void notepad_Load(object sender, EventArgs e)
        {
            ContextMenuStrip = contextMenuStrip1;
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Cut();
            }
        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Copy();
            }
        }

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Paste();
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Название Программы: BHOS Notepad V1.1" + Environment.NewLine + "Cоздатель:BorovHackMachine" +
                Environment.NewLine + "Версия программы: V1.1" + Environment.NewLine +
                "Cвязь с создателем: vk.com/rf_demon", "Информация о приложении!", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox rtb = null;
            if (tabControl1.TabCount != 0)
            {
                rtb = tabControl1.SelectedTab.Controls[0] as RichTextBox;
                rtb.Clear();
            }
        }

        private void оСоздателеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Создатель: BorovHackMachine" + Environment.NewLine + "Помощь в разработке: CasperSC" +
                Environment.NewLine + "Огромное спасибо CasperSC", "Информация о разработчиках", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void шрифтToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }
    }
}