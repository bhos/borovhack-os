﻿using System;
using System.IO;
using WinFormsApplication.Data;

namespace WinFormsApplication.Config
{
    /// <summary>
    /// Класс для управления кофигурацией приложений
    /// </summary>
    public class LocalConfig
    {
        private static readonly LocalConfig _instance;

        static LocalConfig()
        {
            _instance = new LocalConfig();
        }

        private LocalConfig()
        {
        }

        /// <summary>
        /// Единственный экземпляр объекта в памяти
        /// </summary>
        public static LocalConfig Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Настройки приложения
        /// </summary>
        public AppSettings Settings { get; private set; }

        /// <summary>
        /// Сохранить текущие настройки
        /// </summary>
        public void SaveCurrentSettings()
        {
            try
            {
                DataSerializer<AppSettings>.SaveObject(Settings, AppContext.Instance.SettingsFile);
            }
            catch (DirectoryNotFoundException)
            {
                AppContext.Instance.CheckFolders();
                try
                {
                    DataSerializer<AppSettings>.SaveObject(Settings, AppContext.Instance.SettingsFile);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        /// <summary>
        /// Загрузить текущие настройки. Загруженные настройки будут доступны из свойства <see cref="Settings"/>
        /// </summary>
        public void LoadCurrentSettings()
        {
            AppSettings appSettings;

            if (File.Exists(AppContext.Instance.SettingsFile))
            {
                try
                {
                    appSettings = DataSerializer<AppSettings>.LoadObject(AppContext.Instance.SettingsFile);
                }
                catch (InvalidOperationException)
                {
                    File.Delete(AppContext.Instance.SettingsFile);
                    appSettings = new AppSettings();
                }
            }
            else
            {
                appSettings = new AppSettings();
            }

            Settings = appSettings;
        }
    }
}
